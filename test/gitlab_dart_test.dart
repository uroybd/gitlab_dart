import 'package:gitlab_dart/gitlab_dart.dart';
import 'package:test/test.dart';

void main() {
  group('Projects', () {
    GitlabAPI api;

    setUp(() {
      api = GitlabAPI();
    });

    test('Test Single Project Get (Without Auth)', () async {
      var data = await api.getProjectByID(11592997);
      // print(data.name);
      expect(data.name, "bnbijoytounicode");
    });

    test('Test Projects Get (Without Auth)', () async {
      var data = await api.getProjects();
      // print(data.length);
      expect(data.length, 20);
    });

    test('Test Single Project Users (Without Auth)', () async {
      var data = await api.getProjectUsers(11592997);
      // print(data.name);
      expect(data.length > 0, true);
    });
  });
}
