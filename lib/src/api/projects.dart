import 'package:gitlab_dart/gitlab_dart.dart';

/// Mixin for the project branch of the API
mixin ProjectAPIMixin on BaseAPI {
  static final String projectBaseURL = "/projects";

  /// Get singla project object by ID:
  Future<Project> getProjectByID(int id, [simple = true]) async {
    var url = "$projectBaseURL/$id/";
    try {
      var data = await this.mapFromGet(url);
      return Project.fromJSON(data, true);
    } catch (e) {
      rethrow;
    }
  }

  /// Get All projects.
  Future<List<Project>> getProjects([Map<String, dynamic> params]) async {
    bool isSimple;
    if (params != null) {
      isSimple = params['simple'] ?? false;
    }
    if (this.authToken == null) {
      isSimple = true;
    }

    var url = projectBaseURL;
    try {
      var data = await listFromGet(url, params: params);
      // print(data);
      return List<Project>.of(
          data.map((item) => new Project.fromJSON(item, isSimple)));
    } catch (e) {
      rethrow;
    }
  }

  /// Get Project Users.
  Future<List<ProjectUserRelation>> getProjectUsers(int id) async {
    var url = "$projectBaseURL/$id/users/";
    try {
      var data = await listFromGet(url);
      // print(data);
      return List<ProjectUserRelation>.of(
          data.map((item) => new ProjectUserRelation.fromJSON(item)));
    } catch (e) {
      rethrow;
    }
  }
}
