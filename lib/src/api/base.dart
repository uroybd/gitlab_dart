import 'dart:convert';

import 'package:gitlab_dart/gitlab_dart.dart';
import 'package:http/http.dart' as http;

/// A mixin to provide the default functionalities of our client.
///
/// Here is a good place to drop a few lines about how we're archetecting our software.
/// Apart from this (BaseAPI) mixin, we'll have mixins for every major 'branches' of the API
/// and compose a default client combining them. However, one can choose mixins selectively to
/// create a limited but lightweight client.
mixin BaseAPI {
  String _apiURL;
  Token authToken;

  set apiURL(String url) => this._apiURL =
      url[url.length - 1] == "/" ? url.substring(0, url.length - 1) : url;

  String get apiURL => this._apiURL;

  /// A method to generate URL parameters from map of data.
  String generateParams(Map<String, dynamic> params) {
    if (params == null || params.isEmpty) {
      return '';
    } else {
      List<String> paramStrings = [];
      params.forEach((key, val) => paramStrings.add("$key=$val"));
      return paramStrings.join("&");
    }
  }

  /// A method to generate URLs.
  String generateURL(String url, {Map<String, dynamic> params}) {
    var paramsString = (params != null && params.isNotEmpty)
        ? this.generateParams(params)
        : null;
    String rurl =
        "${this.apiURL}$url${(paramsString != null) ? '?${paramsString}' : ''}";
    return rurl;
  }

  /// Method to generate header programitically from data and inject authetications.
  Map<String, String> getHeader(
      {bool auth = true, Map<String, String> addHeaders}) {
    Map<String, String> headers = addHeaders != null ? addHeaders : {};
    if (auth) {
      if (this.authToken != null) {
        if (this.authToken.type == "Oauth2") {
          headers['Authorization'] = "Bearer ${this.authToken.token}";
        } else if (this.authToken.type == "personal") {
          headers['Private-Token'] = this.authToken.token;
        }
      }
    }
    return headers;
  }

  /// To return Map data from get request.
  Future<Map<String, dynamic>> mapFromGet(String url,
      {Map<String, dynamic> params,
      bool auth = true,
      Map<String, String> addHeaders}) async {
    var client = http.Client();
    try {
      var headers = this.getHeader(auth: auth, addHeaders: addHeaders);
      var gurl = generateURL(url, params: params);
      var resp = await client.get(gurl, headers: headers);
      // print(resp);
      return json.decode(utf8.decode(resp.bodyBytes));
    } catch (e) {
      rethrow;
    }
  }

  /// To return List of data from get request.
  Future<List<dynamic>> listFromGet(String url,
      {Map<String, dynamic> params,
      bool auth = true,
      Map<String, String> addHeaders}) async {
    var client = http.Client();
    try {
      var headers = this.getHeader(auth: auth, addHeaders: addHeaders);
      var resp =
          await client.get(generateURL(url, params: params), headers: headers);
      // print("$resp");
      return json.decode(utf8.decode(resp.bodyBytes));
    } catch (e) {
      // print("$e");
      rethrow;
    }
  }
}
