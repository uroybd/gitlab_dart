import 'package:gitlab_dart/gitlab_dart.dart';
import 'package:gitlab_dart/src/api/projects.dart';

/// Default API client. Phew!
class GitlabAPI with BaseAPI, ProjectAPIMixin {
  String apiURL;
  Token authToken;

  GitlabAPI({this.authToken, apiURL = "https://gitlab.com/api/v4"}) {
    this.apiURL = apiURL;
  }
}
