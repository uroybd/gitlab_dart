part of gitlab_dart;

class ProjectVisibility {
  static const private = "private";
  static const internal = "internal";
  static const public = "public";
}

class ProjectMergeMethod {
  static const merge = "merge";
  static const rebaseMerge = "rebase_merge";
  static const fastForwarded = "ff";
}

class ProjectOrder {
  static const id = "id";
  static const name = "name";
  static const path = "path";
  static const createdAt = "created_at";
  static const updatedAt = "updated_at";
  static const lastActivityAt = "last_activity_at";
}

class ProjectSort {
  static const ascending = "asc";
  static const descending = "desc";
}
