/// All the classes related to the projects branch of gitlab API.
// part of gitlab_dart;
import 'package:http/http.dart' as http;

import 'package:gitlab_dart/gitlab_dart.dart';

/// A class to represent license objects found in projects.
class License {
  String key;
  String name;
  String nickname;
  String htmlURL;
  String sourceURL;

  License({this.key, this.name, this.nickname, this.htmlURL, this.sourceURL});

  License.fromJSON(Map<String, dynamic> data)
      : key = data['key'],
        name = data['name'],
        nickname = data['nickname'],
        htmlURL = data['html_url'],
        sourceURL = data['source_url'];
}

/// A class to represent a user's relation with a project.
class ProjectUserRelation {
  ProjectUser user;
  String state;

  int get id => user.id;
  String get username => user.username;
  String get name => user.name;
  String get avaterURL => user.avatarURL;
  String get webURL => user.webURL;

  ProjectUserRelation({this.user, this.state});

  ProjectUserRelation.fromJSON(Map<String, dynamic> data)
      : user = new ProjectUser.fromJSON(data),
        state = data['state'];
}

/// Project Owner class.
class Owner {
  // TODO: May have to move somewhere else;

  int id;
  String name;
  DateTime createdAt;

  Owner({this.id, this.name, this.createdAt});

  Owner.fromJSON(Map<String, dynamic> data)
      : id = data['id'],
        name = data['name'],
        createdAt = DateTime.parse(data['created_at']);
}

/// Class to represent group.
class Group {
  int id;
  String name;
  String fullPath;

  Group({this.id, this.name, this.fullPath});

  Group.fromJSON(Map<String, dynamic> data, [prefix = ''])
      : id = data['${prefix}_id'],
        name = data['${prefix}_name'],
        fullPath = data['${prefix}_full_path'];
}

/// Class to represent a group's access to a project.
class GroupAccess {
  Group group;
  int accessLevel;

  int get id => group.id;
  String get name => group.name;
  String get fullPath => group.fullPath;

  GroupAccess({this.group, this.accessLevel});

  GroupAccess.fromJSON(Map<String, dynamic> data)
      : group = new Group.fromJSON(data, 'group'),
        accessLevel = data['group_access_level'];
}

/// A class to represent project namespace.
class Namespace {
  int id;
  String name;
  String path;
  String kind;
  String fullPath;

  Namespace({this.id, this.name, this.path, this.kind, this.fullPath});

  Namespace.fromJSON(Map<String, dynamic> data)
      : id = data['id'],
        name = data['name'],
        path = data['path'],
        kind = data['kind'],
        fullPath = data['full_path'];
}

/// A class for metalinks in projects
class ProjectLinks {
  String self;
  String issues;
  String mergeRequests;
  String repoBranches;
  String labels;
  String events;
  String members;

  ProjectLinks(
      {this.self,
      this.issues,
      this.mergeRequests,
      this.repoBranches,
      this.events,
      this.labels,
      this.members});

  ProjectLinks.fromJSON(Map<String, dynamic> data)
      : self = data['self'],
        issues = data['issues'],
        mergeRequests = data['merge_requests'],
        repoBranches = data['repo_branches'],
        labels = data['labels'],
        events = data['events'],
        members = data['members'];
}

/// A class for project statistics
class ProjectStatistics {
  int commits;
  int storageSize;
  int repositorySize;
  int wikiSize;
  int lfsObjectsSize;
  int jobArtifactsSize;
  int packagesSize;

  ProjectStatistics(
      {this.commits,
      this.storageSize,
      this.repositorySize,
      this.wikiSize,
      this.lfsObjectsSize,
      this.jobArtifactsSize,
      this.packagesSize});

  ProjectStatistics.fromJSON(Map<String, dynamic> data)
      : commits = data['commit_count'],
        storageSize = data['storage_size'],
        repositorySize = data['repository_size'],
        wikiSize = data['wiki_size'],
        lfsObjectsSize = data['lfs_objects_size'],
        jobArtifactsSize = data['job_artifacts_size'],
        packagesSize = data['packages_size'];
}

/// A class to represent access permission.
class AccessPermission {
  int accessLevel;
  int notificationLevel;

  AccessPermission({this.accessLevel, this.notificationLevel});

  AccessPermission.fromJSON(Map<String, dynamic> data)
      : accessLevel = data['access_level'],
        notificationLevel = data['notification_level'];
}

/// This class produces an object with all project and group permissions.
class ProjectPermission {
  AccessPermission projectAccess;
  AccessPermission groupAccess;

  ProjectPermission({this.projectAccess, this.groupAccess});

  ProjectPermission.fromJSON(Map<String, dynamic> data)
      : projectAccess = new AccessPermission.fromJSON(data['project_access']),
        groupAccess = new AccessPermission.fromJSON(data['group_access']);
}

/// The Project Class.
///
/// Brace yourself, it's long.
class Project {
  /// Valid values to represent access levels for (issuesAccessLevel, mergeRequestsAccessLevel,
  /// buildsAccessLevel, wikiAccessLevel, snippetsAccessLevel)
  static final List<String> accessLevels = ["enabled", "disabled", "private"];
  static final List<String> visibilityLevels = [
    "internal",
    "public",
    "private"
  ];

  /// This is not a field from the API, it is a field for internal validations;
  bool simple; // Set by APIClient

  // Simple Fields
  int id;
  String description;
  String defaultBranch;
  String sshURL;
  String httpURL;
  String webURL;
  String readmeURL;
  List<String> tagList;
  String name;
  String nameWithNamespace;
  String path;
  String pathWithNamespace;
  DateTime createdAt;
  DateTime lastActivityAt;
  int forks;
  int stars;
  String avatarURL;

  /// Can be one of internal, public and private.
  String _visibility;

  /// Check and set [_visibility]
  set visibility(String value) {
    if (visibilityLevels.contains(value)) {
      this._visibility = value;
    }
  }

  /// Get [_visibility]
  String get visibility => _visibility;
  // Deep Fields;
  Owner owner;
  int openIssues;

  /// Can be one of enabled, disabled and public.
  String _repositoryAccessLevel;

  /// Check and set [_repositoryAccessLevel]
  set repositoryAccessLevel(String value) {
    if (accessLevels.contains(value)) {
      this._repositoryAccessLevel = value;
    }
  }

  /// Get [_repositoryAccessLevel]
  String get repositoryAccessLevel => this._repositoryAccessLevel;

  /// Can be one of enabled, disabled and public.
  String _issuesAccessLevel;

  /// Check and set [_issuesAccessLevel]
  set issuesAccessLevel(String value) {
    if (accessLevels.contains(value)) {
      this._issuesAccessLevel = value;
    }
  }

  /// Get [_issuesAccessLevel]
  String get issuesAccessLevel => this._issuesAccessLevel;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter gets boolean value based on [_issuesAccessLevel].
  bool get issuesEnabled =>
      (this._issuesAccessLevel == "enabled") ? true : false;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter sets [_issuesAccessLevel] based on boolean value.
  set issuesEnabled(bool val) =>
      this._issuesAccessLevel = (val) ? "enabled" : "disabled";

  /// Can be one of enabled, disabled and public.
  String _mergeRequestsAccessLevel;

  /// Check and set [_mergeRequestsAccessLevel]
  set mergeRequestsAccessLevel(String value) {
    if (accessLevels.contains(value)) {
      this._mergeRequestsAccessLevel = value;
    }
  }

  /// Get [_mergeRequestsAccessLevel]
  String get mergeRequestsAccessLevel => this._mergeRequestsAccessLevel;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter gets boolean value based on [_mergeRequestsAccessLevel].
  bool get mergeRequestsEnabled =>
      (this._mergeRequestsAccessLevel == "enabled") ? true : false;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter sets [_mergeRequestsAccessLevel] based on boolean value.
  set mergeRequestsEnabled(bool val) =>
      this._mergeRequestsAccessLevel = (val) ? "enabled" : "disabled";

  /// Can be one of enabled, disabled and public.
  String _buildsAccessLevel;

  /// Check and set [_buildsAccessLevel]
  set buildsAccessLevel(String value) {
    if (accessLevels.contains(value)) {
      this._buildsAccessLevel = value;
    }
  }

  /// Get [_buildsAccessLevel]
  String get buildsAccessLevel => this._buildsAccessLevel;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter gets boolean value based on [_buildsAccessLevel].
  bool get jobsEnabled => (this._buildsAccessLevel == "enabled") ? true : false;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter sets [_buildsAccessLevelk] based on boolean value.
  set jobsEnabled(bool val) =>
      this._buildsAccessLevel = (val) ? "enabled" : "disabled";

  /// Can be one of enabled, disabled and public.
  String _wikiAccessLevel;

  /// Check and set [_wikiAccessLevel]
  set wikiAccessLevel(String value) {
    if (accessLevels.contains(value)) {
      this._wikiAccessLevel = value;
    }
  }

  /// Get [_wikiAccessLevel]
  String get wikiAccessLevel => this._wikiAccessLevel;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter gets boolean value based on [_wikiAccessLevel].
  bool get wikiEnabled => (this._wikiAccessLevel == "enabled") ? true : false;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter sets [_wikiAccessLevel] based on boolean value.
  set wikiEnabled(bool val) =>
      this._wikiAccessLevel = (val) ? "enabled" : "disabled";

  /// Can be one of enabled, disabled and public.
  String _snippetsAccessLevel;

  /// Check and set [_snippetsAccessLevel]
  set snippetsAccessLevel(String value) {
    if (accessLevels.contains(value)) {
      this._snippetsAccessLevel = value;
    }
  }

  /// Get [_snippetsAccessLevel]
  String get snippetsAccessLevel => this._snippetsAccessLevel;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter gets boolean value based on [_snippetsAccessLevel].
  bool get snippetsEnabled =>
      (this._snippetsAccessLevel == "enabled") ? true : false;

  /// Deprecated. Not available in posts. However, can appear in get requests.
  /// This getter sets [_snippetsAccessLevel] based on boolean value.
  set snippetsEnabled(bool val) =>
      this._snippetsAccessLevel = (val) ? "enabled" : "disabled";

  bool resolveOutdatedDiffDiscussions;
  bool containerRegistryEnabled;
  int creatorid;
  Namespace namespace;
  String importStatus;
  String importError;
  bool sharedRunnersEnabled;
  String runnersToken;
  int ciDefaultGitDepth;
  bool publicJobs;
  List<GroupAccess> sharedWithGroup;
  bool onlyAllowMergeIfPiplineSucceeds;
  bool onlyAllowMergeIfAllDiscussionsAreResolved;
  bool requestAccessEnabled;
  String mergeMethod;
  ProjectStatistics statistics;
  ProjectPermission permissions;
  int approvalsBeforeMerge;
  Project forkedFromProject;

  // Available in details only
  String licenseURL;
  License license;
  String repositoryStorage;

  Project(this.name, this.path,
      {this.id,
      this.description,
      this.defaultBranch,
      this.sshURL,
      this.httpURL,
      this.webURL,
      this.readmeURL,
      this.tagList,
      this.nameWithNamespace,
      this.pathWithNamespace,
      this.createdAt,
      this.lastActivityAt,
      this.forkedFromProject,
      this.forks,
      this.approvalsBeforeMerge,
      this.avatarURL,
      this.ciDefaultGitDepth,
      this.containerRegistryEnabled,
      this.creatorid,
      this.importError,
      this.importStatus,
      this.mergeMethod,
      this.namespace,
      this.onlyAllowMergeIfAllDiscussionsAreResolved,
      this.onlyAllowMergeIfPiplineSucceeds,
      this.openIssues,
      this.owner,
      this.permissions,
      this.publicJobs,
      this.requestAccessEnabled,
      this.resolveOutdatedDiffDiscussions,
      this.runnersToken,
      this.sharedRunnersEnabled,
      this.sharedWithGroup,
      this.simple,
      this.stars,
      this.statistics,
      bool issuesEnabled,
      bool jobsEnabled,
      bool mergeRequestsEnabled,
      bool wikiEnabled,
      bool snippetsEnabled,
      String buildsAccessLevel,
      String issuesAccessLevel,
      String mergeRequestsAccessLevel,
      String wikiAccessLevel,
      String snippetsAccessLevel,
      String repositoryAccessLevel,
      String visibility}) {
    this.jobsEnabled = jobsEnabled;
    this.issuesEnabled = issuesEnabled;
    this.mergeRequestsEnabled = mergeRequestsEnabled;
    this.wikiEnabled = wikiEnabled;
    this.snippetsEnabled = snippetsEnabled;

    this.buildsAccessLevel = buildsAccessLevel;
    this.issuesAccessLevel = issuesAccessLevel;
    this.mergeRequestsAccessLevel = mergeRequestsAccessLevel;
    this.wikiAccessLevel = wikiAccessLevel;
    this.snippetsAccessLevel = snippetsAccessLevel;
    this.repositoryAccessLevel = repositoryAccessLevel;
    this.visibility = visibility;
  }

  Project.fromJSON(Map<String, dynamic> data, [this.simple = true]) {
    id = data['id'];
    description = data['description'];
    defaultBranch = data['default_branch'];
    sshURL = data['ssh_url'];
    webURL = data['web_url'];
    readmeURL = data['readme_url'];
    tagList = List<String>.from(data['tag_list']);
    name = data['name'];
    nameWithNamespace = data['name_with_namespace'];
    path = data['path'];
    pathWithNamespace = data['path_with_namespace'];
    createdAt = DateTime.parse(data['created_at']);
    lastActivityAt = DateTime.parse(data['last_activity_at']);
    forks = data['fork_count'];
    stars = data['star_count'];
    avatarURL = data['avatarURL'];
    visibility = data['visibility'];
    if (!this.simple) {
      owner = new Owner.fromJSON(data['owner']);
      issuesEnabled = data['issue_enabled'];
      openIssues = data['openIssues'];
      mergeRequestsEnabled = data['merge_request_enabled'];
      jobsEnabled = data['jobs_enabled'];
      wikiEnabled = data['wiki_enabled'];
      snippetsEnabled = data['snippets_enabled'];
      resolveOutdatedDiffDiscussions =
          data['resolve_outdated_diff_discussions'];
      containerRegistryEnabled = data['container_registry_enabled'];
      creatorid = data['creator_id'];
      namespace = new Namespace.fromJSON(data['namespace']);
      importStatus = data['import_status'];
      importError = data['import_error'];
      sharedRunnersEnabled = data['shared_runner_enabled'];
      runnersToken = data['runners_token'];
      ciDefaultGitDepth = data['ci_default_git_depth'];
      publicJobs = data['public_jobs'];
      sharedWithGroup = List<GroupAccess>.of(
          (data['shared_with_group'] as Iterable)
              .map((item) => new GroupAccess.fromJSON(item)));
      onlyAllowMergeIfAllDiscussionsAreResolved =
          data['only_allow_merge_if_all_discussions_are_resolved'];
      onlyAllowMergeIfPiplineSucceeds =
          data['only_allow_merge_if_pipeline_succeeds'];
      requestAccessEnabled = data['request_access_enabled'];
      mergeMethod = data['merge_method'];
      statistics = new ProjectStatistics.fromJSON(data['statistics']);
      permissions = new ProjectPermission.fromJSON(data['permissions']);
      approvalsBeforeMerge = data['approvals_before_merege'];
      forkedFromProject = data['forked_from_prject'] ??
          new Project.fromJSON(data['forked_from_project'], true);
      licenseURL = data['license_url'];
      license = new License.fromJSON(data['license']);
      repositoryStorage = data['repository_storage'];

      this.buildsAccessLevel = data['buids_access_level'];
      this.issuesAccessLevel = data['issues_access_level'];
      this.mergeRequestsAccessLevel = data['merge_requests_access_level'];
      this.wikiAccessLevel = data['wiki_access_level'];
      this.snippetsAccessLevel = data['snippets_access_level'];
      this.repositoryAccessLevel = data['repository_access_level'];
    }
  }

  /// A method to generate post data.
  Map<String, dynamic> postData(
      {String importURL,
      bool lfsEnabled,
      bool printingMergeRequestLinkEnabled,
      String buildGitStrategy,
      int buildTimeout,
      String autoCancelPendingPipeline,
      String buildCoverageRegex,
      String ciConfigPath,
      bool autoDevOpsEnabled,
      String autoDevOpsDeployStrategy,
      String externalAuthorizationClassificationLevel,
      bool mirror,
      bool mirrorTriggerBuilds,
      bool onlyMirrorProtectedBranches,
      bool mirrorOverwritesDivergedBranches,
      bool packagesEnabled,
      http.MultipartFile avatar}) {
    if (this.name == null || this.path == null) {
      throw new AssertionError(
          "name and path are mandatory to create a project.");
    }
    Map<String, dynamic> data = {
      "name": name,
      "path": path,
      "namespace_id": namespace?.id,
      "default_branch": defaultBranch,
      "description": description,
      "issues_access_level": issuesAccessLevel,
      "repository_access_level": repositoryAccessLevel,
      "merge_requests_access_level": mergeRequestsAccessLevel,
      "builds_access_level": buildsAccessLevel,
      "wiki_access_level": wikiAccessLevel,
      "snippets_access_level": snippetsAccessLevel,
      "resolve_outdated_diff_discussions": resolveOutdatedDiffDiscussions,
      "shared_runners_enabled": sharedRunnersEnabled,
      "visibility": visibility,
      "public_builds": publicJobs,
      "import_url": importURL,
      "only_allow_merge_if_pipeline_succeeds": onlyAllowMergeIfPiplineSucceeds,
      "only_allow_merge_if_all_discussions_are_resolved":
          onlyAllowMergeIfAllDiscussionsAreResolved,
      "merge_method": mergeMethod,
      "lfs_enabled": lfsEnabled,
      "request_access_enabled": requestAccessEnabled,
      "tag_list": tagList,
      "printing_merge_request_link_enabled": printingMergeRequestLinkEnabled,
      "build_git_strategy": buildGitStrategy,
      "build_timeout": buildTimeout,
      "auto_cancel_pending_pipelines": autoCancelPendingPipeline,
      "build_coverage_regex": buildCoverageRegex,
      "ci_config_path": ciConfigPath,
      "auto_devops_enabled": autoDevOpsEnabled,
      "auto_devops_deploy_strategy": autoDevOpsDeployStrategy,
      "repository_storage": repositoryStorage,
      "approvals_before_merge": approvalsBeforeMerge,
      "external_authorization_classification_label":
          externalAuthorizationClassificationLevel,
      "mirror": mirror,
      "mirror_trigger_builds": mirrorTriggerBuilds,
      "only_mirror_protected_branches": onlyMirrorProtectedBranches,
      "mirror_overwrites_diverged_branches": mirror,
      "packages_enabled": packagesEnabled,
      "avater": avatar
    };
    data.removeWhere((key, val) => val == null);
    return data;
  }
}
