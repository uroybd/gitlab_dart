part of gitlab_dart;

/// A Class to represent Gitlab Token and its type.
class Token {
  String token;
  String type;

  Token(this.token, [this.type = "OAuth2"]);
}
