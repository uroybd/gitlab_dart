part of gitlab_dart;

/// A BaseUser class to create diversive user classes.
abstract class BaseUser {
  // Created for Projects, may have to extend or
  // rename later;
  int id;
  String username;
  String name;
  String avatarURL;
  String webURL;

  BaseUser({this.id, this.username, this.avatarURL, this.webURL, this.name});
}

/// A user class found in projects.
class ProjectUser extends BaseUser {
  ProjectUser(
      {int id, String username, String avatarURL, String webURL, String name})
      : super(
            id: id,
            username: username,
            avatarURL: avatarURL,
            webURL: webURL,
            name: name);

  ProjectUser.fromJSON(Map<String, dynamic> data)
      : super(
            id: data['id'],
            username: data['username'],
            name: data['name'],
            avatarURL: data['avatar_url'],
            webURL: data['web_url']);
}
