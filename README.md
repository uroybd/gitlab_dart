![https://pub.dev/packages/gitlab_dart](https://img.shields.io/pub/v/gitlab_dart.svg)

# Gitlab Dart Client
A Gitlab API client for dart.

### Warning! Still work in progress!
#### PR will be much appreciated.

## Usage

A simple usage example:

```dart
import 'package:gitlab_dart/gitlab_dart.dart';

main() {
  var api = GitlabAPI();
  api.getProjectByID(11592997).then((project) {
    print(project.name);
  });
}
```
