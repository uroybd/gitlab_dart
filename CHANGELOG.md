## 0.1.2
### WIP
- Added postData method in [Projects] class.
- Updated documentations.


## 0.1.1

- Did some changes as suggested by pub analysis, still WIP

## 0.1.0

- Initial version, still WIP
